import {theme_layout} from '../config';

const store = {
    state: {
      data_login: {
        username: '',
        email: '',
        _token: ''
      },
      login: false
    },
    mutations: {
      setUser(state, payload) {
        state.data_login.username = payload.username;
        state.data_login.email = payload.email;
        state.data_login._token = payload.token;
        state.login = true;
      },
      setLogin(state, value){
        state.login = value;
        theme_layout(state)
      },
    }
  }


  export default store;
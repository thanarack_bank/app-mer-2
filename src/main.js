// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import Vuex from "vuex";
import router from "./router";
import VueResource from "vue-resource";
import VueCookies from 'vue-cookies';
import { theme_layout } from './config';
import store_data from './store';

Vue.use(VueCookies);
Vue.use(VueResource);
Vue.use(Vuex);

Vue.config.productionTip = false;

const store = new Vuex.Store(store_data);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,
  router,
  template: "<App/>",
  components: { App },
  created: function () {
    const c_username = this.$cookies.get('username');
    const c_email = this.$cookies.get('email');
    const c_token = this.$cookies.get('_token');
    if (c_email && c_token) {
      this.$store.commit('setUser', {
        username: c_username,
        email: c_email,
        c_token: c_token,
      });
    }
    theme_layout(this.$store.state)
  }
});
export const config = {
  URL_CREATE_NEW_USER: "/api/users",
  URL_LOGIN: "/api/users/login",
  URL_REMOVE_ACCOUNT: "/api/users/remove_account",
  URL_GET_ACCOUNT: "/api/users/account",
  URL_POST_SOCIAL: "/api/users/post_social",
  URL_GET_USER_GROUPS: "/api/users/get_groups",
  URL_USER_ADD_FACEBOOK: "/api/users/auth/facebook"
};

export const theme_layout = function (state) {
  let style;
  let body = $('body');
  if (!state.login) {
    style = {
      bClass: "vertical-layout vertical-menu-modern 1-column menu-expanded fixed-navbar",
      bCol: "1-column"
    };
  } else {
    style = {
      bClass: "vertical-layout vertical-menu-modern 2-columns menu-expanded fixed-navbar",
      bCol: "2-column"
    };
  }
  body.attr('class', style.bClass);
  body.attr('data-col', style.bCol);
  //re_login(state);
}

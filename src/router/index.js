import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';
import {Settings, Info, Account, Package, Password} from '@/components/Settings/index.js';
import {Posts, Newpost} from '@/components/Posts/index.js';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/register',
      name: 'Register'
    },
    {
      path: '/posts',
      name: 'Posts',
      component: Posts,
      children: [{
        path: 'new-post',
        component: Newpost
      }]
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      children: [
        {
          path: 'info',
          component: Info
        },
        {
          path: 'account',
          component: Account
        },
        {
          path: 'package',
          component: Package
        },
        {
          path: 'password',
          component: Password
        }
      ]
    }
  ]
})

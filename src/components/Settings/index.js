import Settings from '@/components/Settings/Settings';
import Info from '@/components/Settings/Info';
import Account from '@/components/Settings/Account';
import Package from '@/components/Settings/Package';
import Password from '@/components/Settings/Password';

export {Settings, Info, Account, Package, Password}

import { config } from '../../config';

export default {
  props: ['data_login'],
  data: function () {
    return {
      running: false,
      error_message: '',
      error_login: false,
    }
  },
  computed: {
    btn_class: function () {
      let class_name = "btn btn-primary btn-lg ld-ext-right";
      if (this.running === true) class_name += " disabled running";
      return class_name;
    },
  },
  methods: {
    dismissAlert: function () {
      this.error_login = false;
    },
    AuthLogin: function () {
      let _this = this;
      _this.running = true;
      _this.error_login = false;
      const obj = {
        email: _this.data_login.email,
        password: _this.data_login.password
      };
      if (!obj.email || !obj.password) {
        _this.error_message = "ไม่พบอีเมล์และรหัสผ่าน";
        _this.error_login = true;
        setTimeout(function () {
          _this.running = false;
        }, 1000);
      } else {
        _this.$http.post(config.URL_LOGIN, obj, {}).then(function (response) {
          if (response.body.status === 100) {
            if (response.body.result.active === 1) {
              _this.error_login = false;
              _this.$cookies.set('id_user', response.body.result._id, "1d");
              _this.$cookies.set('username', response.body.result.name + ' ' + response.body.result.lastName, "1d");
              _this.$cookies.set('email', response.body.result.email, "1d");
              _this.$cookies.set('_token', response.body.token, "1d");
              return window.location.reload();
            } else {
              _this.error_login = true;
              _this.error_message = 'คุณถูกระงับการใช้งาน';
              setTimeout(function () {
                _this.running = false;
              }, 2500);
            }
          } else {
            _this.error_login = true;
            _this.error_message = response.body.message;
            setTimeout(function () {
              _this.running = false;
            }, 2500);
          }
        });
      }
    }
  },
  template: '<div id="i_button_login">' +
  '<div align="center" class="alert alert-danger" v-if="error_login">' +
  '<button type="button" @click="dismissAlert" class="close"  aria-label="Close">' +
  '<span aria-hidden="true">×</span>' +
  '</button>{{ error_message }}</div>' +
  '<button type="button" :class="btn_class"  @click="AuthLogin">' +
  '<i class="ft-unlock"></i> เข้าสู่ระบบ' +
  '<div class="ld ld-ring ld-spin"></div>' +
  '</button></div>',
};

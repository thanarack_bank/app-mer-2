import {config} from '../../config';

export default {
  props: ['data_register'],
  data: function () {
    return {
      running: false,
      error_message: '',
      error_login: false,
    }
  },
  computed: {
    btn_class: function () {
      let class_name = "btn btn-primary btn-lg ld-ext-right";
      if (this.running === true) class_name += " disabled running";
      return class_name;
    },
  },
  methods: {
    dismissAlert: function () {
      this.error_login = false;
    },
    AuthRegister: function () {
      let _this = this;
      _this.running = true;
      _this.error_login = false;
      const obj = {
        name: _this.data_register.name,
        lastName: _this.data_register.lastName,
        email: _this.data_register.email,
        password: _this.data_register.password,
        phone: _this.data_register.phone,
      };
      if (!obj.email || !obj.lastName || !obj.email || !obj.password || !obj.phone) {
        _this.error_message = "กรุณากรอกข้อมูลให้ครบ";
        _this.error_login = true;
        setTimeout(function () {
          _this.running = false;
        }, 1000);
      } else if (obj.password.length < 6) {
        _this.error_message = "รหัสผ่านควรมี 6 ตัวขึ้นไป";
        _this.error_login = true;
        setTimeout(function () {
          _this.running = false;
        }, 1000);
      } else if (obj.password !== _this.data_register.password_confirm) {
        _this.error_message = "รหัสผ่านไม่ตรงกัน";
        _this.error_login = true;
        setTimeout(function () {
          _this.running = false;
        }, 1000);
      } else {
        _this.$http.post(config.URL_CREATE_NEW_USER, obj, {}).then(function (response) {
          if (response.body.status === 100) {
            const objLogin = {
              email: obj.email,
              password: obj.password
            };
            _this.error_login = false;
            _this.$http.post(config.URL_LOGIN, objLogin, {}).then(function (response) {
              _this.$cookies.set('id_user', response.body.result._id, "1d");
              _this.$cookies.set('username', response.body.result.name + ' ' + response.body.result.lastName, "1d");
              _this.$cookies.set('email', response.body.result.email, "1d");
              _this.$cookies.set('_token', response.body.token, "1d");
              return window.location.reload();
            });
            //return _this.$store.commit('setLogin', true, response.body);
          } else {
            _this.error_login = true;
            _this.error_message = response.body.message;
            setTimeout(function () {
              _this.running = false;
            }, 2500);
          }
        });
      }
    }
  },
  template: '<div id="i_button_login">' +
  '<div align="center" class="alert alert-danger" v-if="error_login"><button type="button" @click="dismissAlert" class="close"  aria-label="Close">' +
  '<span aria-hidden="true">×</span>' +
  '</button>{{ error_message }}</div>' +
  '<button type="button" :class="btn_class"  @click="AuthRegister">' +
  '<i class="ft-unlock"></i> สมัครสมาชิก' +
  '<div class="ld ld-ring ld-spin"></div>' +
  '</button></div>',
};

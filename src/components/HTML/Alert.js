import {config} from '../../config';

export default {
  props: ['title', 'message', 'icon', 'show', 'closeBtn'],
  computed: {
    setIconClass: function () {
      return this.icon + ' white font-medium-5';
    },
    open: function () {
      return this.show;
    }
  },
  methods: {
    close: function () {
      this.$emit('update:show', false);
    }
  },
  template: '  <div><transition name="slide-fade"><div v-if="open" class="bs-callout-pink callout-square callout-bordered callout-transparent mb-1">' +
  '<div class="media">' +
  '<div class="media-left media-middle bg-pink callout-arrow-left position-relative px-2">' +
  '<i :class="setIconClass"></i>' +
  '</div>' +
  '<div class="media-body p-1">' +
  '<div class="pull-right" v-if="closeBtn"><button @click="close" type="button" class="btn btn-outline-danger btn-sm"><i class="fa fa-close"></i></button></div>' +
  '<strong class="callout-title">{{title}}</strong>' +
  '<p>{{message}}</p>' +
  '</div>' +
  '</div>' +
  '</div></transition></div>',
};

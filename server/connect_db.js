const mongo_client = require('mongodb').MongoClient;

const connect_db = function () {
    return new Promise(function (resolve, reject) {
        mongo_client.connect('mongodb://localhost:27017/isell', function (err, db) {
            if (err) throw err;
            return resolve(db);
        });
    });
};


module.exports = connect_db;
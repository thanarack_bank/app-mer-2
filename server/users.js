const config = require('./config');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const middleware_token = require('./middleware_token');
const connect_db = require('./connect_db');
const {FB} = require('fb');
const ObjectID = require('mongodb').ObjectID;
const request = require('request');
const md5 = require('md5');

const insert_accounts = function (obj = {}) {
  return new Promise(function (resolve) {
    connect_db().then(function (db) {
      db.collection('accounts').insertOne(obj, function (err, res) {
        // User update account
        db.collection('users').update({_id: new ObjectID(obj.id_user)}, {$push: {account: res.insertedId}});
        // end
        //console.log(res);
        return resolve(res);
      });
    });
  });
}

const check_accounts = function (id_facebook) {
  return new Promise(function (resolve) {
    connect_db().then(function (db) {
      db.collection('accounts').findOne({id_facebook: id_facebook}).then(function (res) {
        return resolve(res);
      });
    });
  });
}

const check_user = function (email, password) {
  return new Promise(function (resolve) {
    connect_db().then(function (db) {
      const obj = {};
      if (email) obj.email = email;
      if (password) obj.password = password;
      db.collection('users').findOne(obj).then(function (res) {
        return resolve(res);
      });
    });
  });
}

const insert_user = function (obj = {}) {
  return new Promise(function (resolve) {
    connect_db().then(function (db) {
      db.collection('users').insertOne(obj).then(function (err, res) {
        return resolve(res);
      });
    });
  });
}

const update_token = function (id_facebook, access_token) {
  return new Promise(function (resolve) {
    connect_db().then(function (db) {
      db.collection('accounts').update({id_facebook: id_facebook}, {$set: {access_token: access_token}}).then(function (res) {
        return resolve(res);
      });
    });
  });
}

const remove_account = function (id_user, id_facebook) {
  return new Promise(function (resolve) {
    connect_db().then(function (db) {
      db.collection('users').update({_id: new ObjectID(id_user)}, {$pull: {account: new ObjectID(id_facebook)}}).then(function (res) {
        db.collection('accounts').remove({
          _id: new ObjectID(id_facebook),
          id_user: new ObjectID(id_user)
        }).then(function (res) {
          return resolve(res);
        });
      });
    });
  });
}

const generate_token = function (username, password) {
  const sig = md5("api_key=" + config.iSellApp.appID_fb_a + "credentials_type=passwordemail=" + username + "format=JSONgenerate_machine_id=1generate_session_cookies=0locale=en_USmethod=auth.loginpassword=" + password + "return_ssl_resources=0v=1.0" + config.iSellApp.appSecret_fb_a);
  const url = "https://api.facebook.com/restserver.php?api_key=" + config.iSellApp.appID_fb_a + "&credentials_type=password&email=" + username + "&format=JSON&generate_machine_id=1&generate_session_cookies=0&locale=en_US&method=auth.login&password=" + password + "&return_ssl_resources=0&v=1.0&sig=" + sig;
  return new Promise(function (resolve, reject) {
    request({url: url}, function (error, response, body) {
      //console.log(body);
      const body_json = JSON.parse(body);
      return resolve(body_json);
    });
  });
}

module.exports = function (app) {

  app.get('/users/account', middleware_token, function (req, res) {
    connect_db().then(function (db) {
      const obj = {
        id_user: new ObjectID(req.token_web.id_user)
      };
      db.collection('accounts').find(obj).toArray(function (err, result) {
        res.status(200);
        if (result !== null) {
          res.json({
            status: 100,
            data: result
          })
        } else {
          res.json({
            status: 200,
            message: "ไม่พบผู้ใช้งาน"
          })
        }
      });
    });
  });

  app.post('/users/genarate_token', function (req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const _res = res;
    generate_token(username, password).then(function (res) {
      //console.log(res);
      _res.status(200);
      if (res.error_code) {
        _res.json({
          status: 200,
          message: 'เกิดข้อผิดพลาด : ' + res.error_msg
        });
      } else {
        _res.json({
          status: 100,
          message: 'เรียกโทเค็นสำเร็จ',
          result: res
        });
      }
    });
  });

  app.post('/users', function (req, res) {
    if (req.body) {
      check_user(req.body.email).then(function (result) {
        res.status(200);
        if (result === null) {
          const obj = {
            name: req.body.name,
            lastName: req.body.lastName,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password),
            phone: req.body.phone,
            active: 1,
            created_at: new Date(),
            updated_at: new Date()
          };
          insert_user(obj).then(function (result) {
            res.json({
              status: 100,
              message: "เพิ่มข้อมูลผู้ใช้งานแล้ว",
              data: result
            });
          });
        } else {
          res.json({
            status: 200,
            message: "ผู้ใช้งานนี้มีอยู่ในระบบแล้ว"
          });
        }
      });
    }
  });


  app.post('/users/auth/facebook', middleware_token, function (req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const _res = res;
    generate_token(username, password).then(function (res) {
      //console.log(res);
      const __res = res;
      _res.status(200);
      if (!res.error_code) {
        FB.setAccessToken(__res.access_token);
        check_accounts(res.uid).then((res) => {
          if (res === null) {
            FB.api(__res.uid.toString(), {fields: ['id', 'name', 'email', 'picture']}, function (fb_res) {
              if (fb_res.id) {
                const obj = {
                  id_user: new ObjectID(req.token_web.id_user),
                  id_facebook: fb_res.id.toString(),
                  account_name: fb_res.name,
                  access_token: __res.access_token,
                  email: fb_res.email,
                  exp: 0,
                  picture: fb_res.picture.data.url,
                  acc_type: 'facebook',
                  created_at: new Date(),
                  updated_at: new Date()
                };
                insert_accounts(obj).then(function (res) {
                  _res.json({
                    status: 100,
                    message: 'เพิ่มบัญชีใหม่เรียบร้อย',
                    access_token: obj.access_token
                  });
                });
              } else {
                _res.json({
                  status: 200,
                  message: 'ไม่พบบัญชีเฟสบุ๊คของคุณ'
                });
              }
            });
          } else {
            _res.json({
              status: 200,
              message: 'บัญชีเฟสบุ็ค ' + __res.identifier + ' ได้ลงทะเบียนไปแล้ว'
            });
          }
        });
      } else if (res.error_code === 613) {
        _res.json({
          status: 200,
          message: 'คุณพยายามเข้าสู่ระบบมากเกินไปกรุณารออีกสักครู่'
        });
      } else {
        _res.json({
          status: 200,
          message: 'คุณกรอกผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง'
        });
      }
      /* else {
       _res.json({
       status: 200,
       message: 'เกิดความผิดพลาด : ' + res.error_code + ' ' + res.error_message
       });
       }*/
    });
  });

  /*app.post('/users/auth/facebook', middleware_token, function (req, res) {
   let epRes = res;
   FB.api('oauth/access_token', {
   client_id: config.iSellApp.appID,
   client_secret: config.iSellApp.appSecret,
   redirect_uri: req.body.redirectUri,
   code: req.body.code
   }, function (res) {
   if (!res || res.error) {
   console.log(res);
   epRes.status = 500;
   epRes.json({
   status: 500,
   message: 'Error occured: ' + res.error
   });
   } else {
   FB.api('oauth/access_token', {
   grant_type: 'fb_exchange_token',
   client_id: config.iSellApp.appID,
   client_secret: config.iSellApp.appSecret,
   fb_exchange_token: res.access_token
   }, function (res) {
   FB.setAccessToken(res.access_token);
   // insert user
   // check user exit
   const cu_res = res;
   FB.api('me?fields=name,email,id,photos', 'GET', function (res) {
   const res_face = res;
   check_accounts(res_face.id).then((res) => {
   if (res === null) {
   const obj = {
   id_user: new ObjectID(req.token_web.id_user),
   id_facebook: res_face.id,
   account_name: res_face.name,
   token_facebook: cu_res.access_token,
   email: res_face.email,
   exp: 0,
   acc_type: 'facebook'
   };
   insert_accounts(obj).then(function (res) {
   epRes.json({
   status: 100,
   message: 'เข้าถึงโทเค็นระยะยาวได้แล้ว',
   access_token: cu_res.access_token,
   result: res_face
   });
   });
   } else {
   update_token(res_face.id, cu_res.access_token).then(function (res) {
   epRes.json({
   status: 200,
   message: 'บัญชีเฟสบุ็ค ' + res_face.name + ' ได้ลงทะเบียนไปแล้ว หากต้องการเลือกบัญชีใหม่กรุณาออกจากเฟสบุ็คก่อน'
   });
   });
   }
   });
   });
   });
   }
   });
   });*/

  app.post('/users/login', function (req, res) {
    var obj = {
      email: req.body.email,
      password: req.body.password
    };

    if (!obj.email || !obj.password) {
      res.json({
        status: 200,
        message: "ไม่พบอีเมล์และรหัสผ่าน"
      });
    }

    check_user(obj.email).then(function (result) {
      if (result !== null) {
        if (bcrypt.compareSync(obj.password, result.password)) {
          var token = jwt.sign({id_user: result._id, email: obj.email}, app.get('app-secret'), {expiresIn: '24h'});
          delete result.password;
          res.json({
            status: 100,
            message: "เข้าสู่ระบบสำเร็จ",
            result: result,
            token: token
          });
        } else {
          res.json({
            status: 200,
            message: "รหัสผ่านไม่ถูกต้อง"
          });
        }
      } else {
        res.json({
          status: 200,
          message: "ไม่พบผู้ใช้งาน"
        });
      }
    });
  });

  app.post('/users/remove_account', middleware_token, function (req, res) {
    const res_app = res;
    remove_account(req.token_web.id_user, req.body.id_facebook).then(function (res) {
      //console.log(req);
      res_app.json({
        status: 100,
        message: "ลบบัญชีสำเร็จ"
      });
    });
  });


  app.post('/users/post_social', middleware_token, function (req, res) {
    const res_app = res;
    check_accounts(req.body.id_account).then(function (res) {
      FB.api('114126945990432/feed?access_token=' + res.access_token, 'post', {message: req.body.txt_message}, function (res) {
        if (!res || res.error) {
          console.log(!res ? 'error occurred' : res.error);
          return;
        }
        res_app.json({
          status: 100,
          message: "โพสข้อความสำเร็จ",
          result: res
        });
      });
    });
  });

  app.get('/users/get_groups', middleware_token, function (req, res) {
    const res_app = res;
    check_accounts(req.query.id_account).then(function (res) {
      console.log(res.access_token);
      FB.api(res.id_facebook + '/groups?access_token=' + res.access_token + '&limit=300', 'get', function (res) {
        if (!res || res.error) {
          console.log(!res ? 'error occurred' : res.error);
          return;
        }
        res_app.json({
          status: 100,
          message: "เรียกรายการกลุ่มสำเร็จแล้ว",
          result: res
        });
      });
    });
  });

};


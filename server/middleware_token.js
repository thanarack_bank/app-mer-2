const mongodb = require('mongodb');
const config = require('./config');
const jwt = require('jsonwebtoken');

const middleWareToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config.token_secret, function (err, decode) {
            if (err) {
                res.json({
                    status: 200,
                    message: "โทเค็นไม่ถูกต้อง"
                })
            } else {
                //console.log(decode);
                req.token_web = decode;
                next();
            }
        })
    } else {
        res.json({
            status: 200,
            message: "ไม่พบโทเค็น"
        });
    }
}


module.exports = middleWareToken;

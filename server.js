const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
const port = 3600;
const bodyParser = require('body-parser');
const config = require('./server/config');
const cors = require('cors');
const moment = require('moment-timezone');
moment.tz.setDefault('Asia/Bangkok');

app.use(cors({ origin: ['http://localhost:3000', 'http://localhost:8080'] }));
app.set('app-secret', config.token_secret);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/static', express.static(path.join(__dirname, './dist/static')));

require('./server/users')(app);

app.get('/', function (req, res) {
    //console.log(connectDB);
    //res.send('Api index');
    //res.sendfile('./dist/index.html');
    res.send('API');
});

console.log('Server is running at : ' + port);

app.listen(port);